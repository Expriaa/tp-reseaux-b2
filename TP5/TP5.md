# TP 5 Sécu défensive : VPN

## I. WireGuard serveur.

### 1. Les firewall.

* On commence par les firewall:

```sudo  dnf install firewalld -y
sudo systemctl start firewalld

sudo systemctl status firewalld

● firewalld.service - firewalld - dynamic firewall daemon
     Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; ve>
     Active: active (running) since Mon 2022-10-17 16:43:59 CEST; 27min ago
...

sudo firewall-cmd --permanent --list-all

sudo firewall-cmd --reload

success
```
- Les firewall fonctionnent.

### 2. Paramétrage de WireGuard server.

1. Les paquets pour WireGuard.
```
sudo dnf install elrepo-release epel-release
```
2. WireGuard.
```
sudo dnf install kmod-wireguard wireguard-tools
```

### 2.1. Paramétrage de WireGuard server (clé).


1. La clé privée pour WireGuard.

```
wg genkey | sudo tee /etc/wireguard/private.key
sAQGBLTbJSOp3u7Fdz4oFtnXrdCdPfVdGEEC/xTSWFM=
```
2. Changer les autorisations de la clé privée.

```
sudo chmod go= /etc/wireguard/private.key
```

3. Maintenant la clé public.

```
sudo cat /etc/wireguard/private.key | wg pubkey | sudo tee /etc/wireguard/public.key
Ep+uvBHVU24BImQNpEGY5T2HWD0YpEFXZxkjRNN3pCY=
```

### 2.2. Paramétrage de WireGuard server (ip range).

1. Configuration de l'ipv4.

- ipv4 = 10.8.0.1/24

### 3. Configuration de l'interface du serveur WireGuard.

```
sudo nano /etc/wireguard/wg0.conf

[Interface]
PrivateKey = iHsDkxOTZ7SmtqdO5Zr9aSDOCBQyLJDbh4QY9WaYXEo=
Address = 10.8.0.1/24
ListenPort = 51820
SaveConfig = true
```

### 4.Configuration réseau du serveur WireGuard.

* Activer l'IP forwarding.

```
sudo nano /etc/sysctl.conf
...
net.ipv4.ip_forward=1
```

```
sudo sysctl -p
net.ipv4.ip_forward = 1
```

### 5. Configuration du pare-feu du serveur WireGuard.

1. Autoriser l'accès permanent au service WireGuard sur le port UDP 51820 :
```
sudo firewall-cmd --zone=public --add-port=51820/udp --permanent
```

2. Ajouter l'interface wg0 à la zone interne
```
 sudo firewall-cmd --zone=internal --add-interface=wg0 --permanent
success
```

3. Activer le masquage de nos plages réseau IPv4:

```
sudo firewall-cmd --zone=public --add-rich-rule='rule family=ipv4 source address=10.8.0.1/24 masquerade' --permanent

sudo firewall-cmd --reload

sudo firewall-cmd --zone=public --list-all

public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 51820/udp 				///vpn port
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
        rule family="ipv4" source address="10.3.1.1/24" masquerade
```

* Il est possible de vérifier que la règle de transfert interne est en place :

```
sudo firewall-cmd --zone=internal --list-interfaces
wg0
```

### 6. Démarrage du serveur WireGuard.

1. Configurer WireGuard pour qu'il démarre au démarrage. 
- Activer le service wg-quick pour que le tunnel wg0 soit ajouté à systemctl :

```
sudo systemctl enable wg-quick@wg0.service
Created symlink /etc/systemd/system/multi-user.target.wants/wg-quick@wg0.service → /usr/lib/systemd/system/wg-quick@.service.
```

1.1. on vas lancer le service "wg-quick" : 
```
sudo systemctl start wg-quick@wg0.service
sudo systemctl status wg-quick@wg0.service

   Active: active (exited) since Tue 2022-10-18 14:50:17 CEST; 5min ago
```

## II. on ajoute un router

### 1. router et ip forward

```
sudo nano /etc/sysctl.d/routeur.conf
net.ipv4.ip_forward=1
```
* Activer l'ip forward.

```
sudo sysctl -p /etc/sysctl.d/routeur.conf
net.ipv4.ip_forward = 1

cat /proc/sys/net/ipv4/ip_forward
1
```

### 2. testons si on peut se ping

```
PING 10.5.2.7 (10.5.2.7) 56(84) bytes of data.
64 bytes from 10.5.2.7: icmp_seq=1 ttl=63 time=0.997 ms
64 bytes from 10.5.2.7: icmp_seq=2 ttl=63 time=2.40 ms
64 bytes from 10.5.2.7: icmp_seq=3 ttl=63 time=2.29 ms
64 bytes from 10.5.2.7: icmp_seq=4 ttl=63 time=2.38 ms
--- 10.5.2.7 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 0.997/2.016/2.399/0.592 ms
```

ping 10.5.1.7
```
PING 10.5.1.7 (10.5.1.7) 56(84) bytes of data.
64 bytes from 10.5.1.7: icmp_seq=1 ttl=63 time=1.16 ms
64 bytes from 10.5.1.7: icmp_seq=2 ttl=63 time=1.41 ms
64 bytes from 10.5.1.7: icmp_seq=3 ttl=63 time=2.14 ms
64 bytes from 10.5.1.7: icmp_seq=4 ttl=63 time=1.36 ms
--- 10.5.1.7 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 1.161/1.519/2.141/0.371 ms
```

## III. WireGuard client.

### 1. Paramétrage de WireGuard client.

```
sudo dnf install elrepo-release epel-release
...
sudo dnf install kmod-wireguard wireguard-tools
...
```

### 2. Paramétrage de WireGuard server (clé).

* La clé privée :
```
wg genkey | sudo tee /etc/wireguard/private.key
mI/vd5yhkf+lesRPqCyowHYotTmTh7/8SzM9z/PwTkk=
sudo chmod go= /etc/wireguard/private.key
```

* La clé publique :
```
sudo cat /etc/wireguard/private.key | wg pubkey | sudo tee /etc/wireguard/public.key
nyB1k3yBewKnxlW47x/20O914uWzwO5uiZbVPxXMZlA=
```

### 3. Paramétrage de WireGuard client.


```
sudo nano /etc/wireguard/wg0.conf

[Interface]
PrivateKey = sAQGBLTbJSOp3u7Fdz4oFtnXrdCdPfVdGEEC/xTSWFM=
Address = 10.8.0.2/24

[Peer]
PublicKey = Ep+uvBHVU24BImQNpEGY5T2HWD0YpEFXZxkjRNN3pCY=
AllowedIPs = 10.8.0.0/24
Endpoint = 10.5.2.7:51820
```

### 4. Configuration du client pour acheminer tout le trafic sur le tunnel



```
ip route list table main default
default via 10.0.2.2 dev enp0s3 proto dhcp src 10.0.2.15 metric 100
default via 10.5.2.254 dev enp0s8 proto static metric 101

ip -brief address show enp0s8
enp0s8           UP             10.5.1.7/24 fe80::a00:27ff:fe32:1a3/64
```

### 5. Configuration du DNS sur le client.


cat /etc/resolv.conf
```
nameserver 8.8.8.8
nameserver 8.8.4.4
nameserver 1.1.1.1
```


```
sudo nano /etc/wireguard/wg0.conf
DNS = 1.1.1.1 8.8.4.4 8.8.8.8

sudo systemctl enable systemd-resolved
sudo reboot
```

## IV Ajout de la clé publique du client au serveur.

### 1. clé public client

```
sudo cat /etc/wireguard/public.key
nyB1k3yBewKnxlW47x/20O914uWzwO5uiZbVPxXMZlA=
```

### 2. depuis le server

```
sudo wg set wg0 peer nyB1k3yBewKnxlW47x/20O914uWzwO5uiZbVPxXMZlA= allowed-ips 10.8.0.2

sudo wg

...
interface: wg0
  public key: 99YkyDO084ggJP9Bq+faFVlfGql/2jK6/4BPMMoaqiY=
  private key: (hidden)
  listening port: 51820

peer: nyB1k3yBewKnxlW47x/20O914uWzwO5uiZbVPxXMZlA=
  allowed ips: 10.8.0.2/32
```

## V. Connexion VPN du client au tunnel vpn.

### 1. connexion au vpn depuis le client.

```
sudo wg-quick up wg0

ip a
...
11: wg0: <POINTOPOINT,NOARP,UP,LOWER_UP> mtu 1420 qdisc noqueue state UNKNOWN group default qlen 1000
    link/none
    inet 10.8.0.1/24 scope global wg0
       valid_lft forever preferred_lft forever
```

## 2. voyons si on peut ping le serveur dns 8.8.8.8 depuis le réseau du vpn.

```
ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=27.1 ms
...

64 bytes from 8.8.8.8: icmp_seq=10 ttl=113 time=19.8 ms

--- 8.8.8.8 ping statistics ---
10 packets transmitted, 10 received, 0% packet loss, time 9019ms
rtt min/avg/max/mdev = 18.721/21.509/27.087/2.599 ms
```


## VI. Traceoute

### 1. traceroute:

```
traceroute 10.5.2.7
traceroute to 10.5.2.7 (10.5.2.7), 30 hops max, 60 byte packets
 1  * * *
 2  * * *
 3  * * *
 4  * * *
 5  * * *
 6  * * *
 7  * * *
 8  * * *
 9  * * *
10  * * *
11  * * client (10.5.1.7)  3085.486 ms !H
```

```
traceroute 10.8.0.1
traceroute to 10.8.0.1 (10.8.0.1), 30 hops max, 60 byte packets
 1  client (10.5.1.7)  3051.361 ms !H  3051.329 ms !H  3051.325 ms !H
```

### 2. les tables ARP

```
sudo arp
Address                  HWtype  HWaddress           Flags Mask            Iface
10.5.2.7                 ether   0a:00:27:00:00:0b   C                     enp0s8
_gateway                 ether   52:54:00:12:35:02   C                     enp0s3
``` 
