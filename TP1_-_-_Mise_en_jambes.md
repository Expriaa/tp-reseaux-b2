# TP1 - - Mise en jambes

## I. Exploration locale en solo

### 1. Affichage d'informations sur la pile TCP/IP locale

🌞 Affichez les infos des cartes réseau de votre PC

```
PS C:\Users\arthu> ipconfig /all

Carte réseau sans fil Wi-Fi :

[...]
Adresse physique . . . . . . . . . . . : D8-FC-93-30-F1-7E
[...]
Adresse IPv4. . . . . . . . . . . . . .: 10.33.18.90(préféré)
[...]
Passerelle par défaut. . . . . . . . . : 10.33.19.254

Carte Ethernet Ethernet :

[...]
Adresse physique . . . . . . . . . . . : 38-63-BB-B8-E5-7F
[...]
Pas d'ip car pas de connection via cable eth
```

🌞 Affichez votre gateway


(Gateway : voir carte Wi-Fi)

🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS)

Pour obtenir ces informations avec l'interface graphique :

Sélectionnez Démarrer > Paramètres > réseau & internet > Wi-Fi, puis sélectionnez le Wi-Fi réseau à partir de qui vous êtes connecté. Elle seront affichées au bas de la page.

🌞 à quoi sert la gateway dans le réseau d'YNOV ?

La gateway du réseau de Ynov permet de se connecter à d'autres réseaux (exemple : internet), un peu comme une passerelle :p

### 2. Modifications des informations

🌞 Utilisez l'interface graphique de votre OS pour changer d'adresse IP 

Pour changer l'ip de DHCP à une ip manuelle il suffit de se rendre dans les paramètres > réseau & internet > Wi-FI, puis séléctionner le réseau sur lequel on est connecté puis séléctionner "Passerelle IPv4" et la passer de automatique (DHCP) à manuelle et entré la nouvelle adresse ip.


```
PS C:\Users\arthu> ipconfig /all

Carte réseau sans fil Wi-Fi :

[...]
Adresse physique . . . . . . . . . . . : D8-FC-93-30-F1-7E
[...]
Adresse IPv4. . . . . . . . . . . . . .: 10.33.18.17(préféré)
```

🌞 Il est possible que vous perdiez l'accès internet. Que ce soit le cas ou non, expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération.

Si notre machine n'a plus accès à internet c'est qu'une autre machine possède déjà cet adresse ip.

## II. Exploration locale en duo

🌞Si vos PCs ont un port RJ45 alors y'a une carte réseau Ethernet associée 

```
PS C:\Users\arthu> ipconfig /all

Carte Ethernet Ethernet :

[...]
Adresse IPv4. . . . . . . . . . . . . .: 192.168.18.1(préféré)
[...]
Masque de sous-réseaux. . . . . . . . . : 255.255.255.252
[...]

PS C:\Windows\system32> ping 192.168.18.2

Envoi d’une requête 'Ping'  192.168.18.2 avec 32 octets de données :
Réponse de 192.168.18.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.18.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.18.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.18.2 : octets=32 temps=1 ms TTL=128

Statistiques Ping pour 192.168.18.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 2ms, Moyenne = 1ms
```

Sorry pour la table ARP j'ai zappé de copier coller le résultat :/ mais sinon la commande c'est :
```
arp -a
```
Et ca sert a connaitre les adresses MAC des autres machines dans un même réseau.

🌞 pour tester la connectivité à internet on fait souvent des requêtes simples vers un serveur internet connu

```
PS C:\Windows\system32> ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=1 ms TTL=128
Réponse de 8.8.8.8 : octets=32 temps=1 ms TTL=128
Réponse de 8.8.8.8 : octets=32 temps=1 ms TTL=128
Réponse de 8.8.8.8 : octets=32 temps=1 ms TTL=128

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 2ms, Moyenne = 1ms



PS C:\Windows\system32> ping 1.1.1.1

Envoi d’une requête 'Ping'  1.1.1.1 avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=1 ms TTL=128
Réponse de 1.1.1.1 : octets=32 temps=1 ms TTL=128
Réponse de 1.1.1.1 : octets=32 temps=1 ms TTL=128
Réponse de 1.1.1.1 : octets=32 temps=1 ms TTL=128

Statistiques Ping pour 1.1.1.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 2ms, Moyenne = 1ms
    
    
PS C:\Windows\system32> ping google.com

Envoi d’une requête 'ping' sur google.com [2a00:1450:4007:80e::200e] avec 32 octets de données :
Réponse de 2a00:1450:4007:80e::200e : temps=11 ms
Réponse de 2a00:1450:4007:80e::200e : temps=10 ms
Réponse de 2a00:1450:4007:80e::200e : temps=11 ms
Réponse de 2a00:1450:4007:80e::200e : temps=9 ms

Statistiques Ping pour 2a00:1450:4007:80e::200e:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 9ms, Maximum = 11ms, Moyenne = 10ms
```

🌞 utiliser un traceroute ou tracert pour bien voir que les requêtes passent par la passerelle choisie (l'autre le PC)

J'ai aussi zappé de te le copier coller je règle ca demain avec Eric mybad.

🌞 sur le PC serveur avec par exemple l'IP 192.168.1.1
🌞 sur le PC client avec par exemple l'IP 192.168.1.2

```
C:\Users\arthu\Downloads\netcat-win32-1.11\netcat-1.11>nc.exe -l -p 8888
cucucuc
oasldg$
bjr
oh nice
JohnLee Sins
on a un petit chat privée
BigBlackClock
on a notre chat
```

🌞 pour aller un peu plus loin

```
C:\Users\arthu\Downloads\netcat-win32-1.11\netcat-1.11>nc.exe -l -p 8888 -s 192.168.137.1

PS C:\Windows\system32> netstat -a -b -n  | select-string 8888

  TCP    192.168.137.1:8888     0.0.0.0:0              LISTENING
  ```
  
🌞 Autoriser les ping

```
PS C:\Windows\system32> netsh advfirewall firewall add rule name="ICMP Allow incoming V4 echo request" protocol=icmpv4:8,any dir=out action=allow
Ok.

```

La commande ci-dessus permet de créer une nouvelle règle dans le firewall de windows qui accepte l'envoie de paquet vers une autre machine

```
PS C:\Windows\system32> ping 192.168.137.2

Envoi d’une requête 'Ping'  192.168.137.2 avec 32 octets de données :
Réponse de 192.168.137.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.137.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.137.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.137.2 : octets=32 temps=1 ms TTL=128
```
Tada ça ping.

🌞 Autoriser le traffic sur le port qu'utilise nc

Alors la il faut aller dans les règles du firewall windows, en créer une nouvelle qui accepte que l'on ecoute sur un port TCP.
Et après ça :

```
C:\Users\arthu\Downloads\netcat-win32-1.11\netcat-1.11>nc.exe -l -p 8888 -s 192.168.137.1

azdd
hello
hehehehehehhh
```

## III. Manipulations d'autres outils/protocoles côté client

🌞Exploration du DHCP, depuis votre PC

```
PS C:\Windows\system32> ipconfig /all

[...]

Carte réseau sans fil Wi-Fi :

[...]
Bail expirant. . . . . . . . . . . . . : jeudi 29 septembre 2022 15:37:51
[...]
Serveur DHCP . . . . . . . . . . . . . : 10.33.19.254
[...]
Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
                                       8.8.4.4
                                       1.1.1.1
```

🌞 trouver l'adresse IP du serveur DNS que connaît votre ordinateur

Voir commande ci-dessus

🌞 utiliser, en ligne de commande l'outil nslookup (Windows, MacOS) ou dig (GNU/Linux, MacOS) pour faire des requêtes DNS à la main

```

PS C:\Windows\system32> .\nslookup.exe google.com

[...]
Nom :    google.com
Addresses:  2a00:1450:4007:808::200e
          216.58.215.46

PS C:\Windows\system32> .\nslookup.exe ynov.com

[...]
Nom :    ynov.com
Addresses:  2606:4700:20::681a:be9
          2606:4700:20::ac43:4ae2
          2606:4700:20::681a:ae9
          104.26.10.233
          104.26.11.233
          172.67.74.226
```

DNS 8.8.8.8 car c'est le serveur DHCP qui me fournit cette adresse lorsque je me connecte.
Il y a plusieurs adresse IP pour le réseau Ynov pour la répartition de charge 

```
PS C:\Windows\system32> .\nslookup.exe 78.74.21.21
[...]

Nom :    host-78-74-21-21.homerun.telia.com
Address:  78.74.21.21

PS C:\Windows\system32> .\nslookup.exe 92.146.54.88
[...]

*** dns.google ne parvient pas à trouver 92.146.54.88 : Non-existent domain
```

Il n'y a donc pas de domaine a cette adresse IP







