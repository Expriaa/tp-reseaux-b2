# TP2 : Ethernet, IP, et ARP


## I. Setup IP



🌞 **Mettez en place une configuration réseau fonctionnelle entre les deux machines**

Mon IP
```
PS C:\Users\arthu> ipconfig /all
[...]

Carte Ethernet Ethernet :
[...]
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.125.1(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.192
[...]
```

notre adresse réseaux :
```
192.168.125.0/26
```

notre adresse broadcast:
```
192.168.125.63/26
```

l'IP de Mathias 

```
mathias@MacBook-Air-de-Mathias ~ % ifconfig

    en5: flags=12563<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
    options=6467<RXCSUM,TXCSUM,VLAN_MTU,TSO4,TSO6,CHANNEL_IO,PARTIAL_CSUM,ZEROINVERT_CSUM>
    ether 00:e0:4c:68:04:2c 
    inet6 fe80::1082:3506:9869:1dbf%en5 prefixlen 64 secured scopeid 0x17 
    inet 192.168.125.2 netmask 0xffffffc0 broadcast 192.168.125.63
    nd6 options=201<PERFORMNUD,DAD>
    media: autoselect (1000baseT <full-duplex>)
    status: active
```



🌞 **Prouvez que la connexion est fonctionnelle entre les deux machines**

```
PS C:\Users\arthu> ping 192.168.125.2

Envoi d’une requête 'Ping'  192.168.125.2 avec 32 octets de données :
Réponse de 192.168.125.2 : octets=32 temps<1ms TTL=64
Réponse de 192.168.125.2 : octets=32 temps<1ms TTL=64
Réponse de 192.168.125.2 : octets=32 temps<1ms TTL=64

Statistiques Ping pour 192.168.125.2:
    Paquets : envoyés = 3, reçus = 3, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
```

🌞 **Wireshark it**


* [ICMP](/Packet%20ping%20tp%202.pcapng)

## II. ARP my bro


🌞 **Check the ARP table**

```
PS C:\Users\arthu> arp -a

Interface : 192.168.125.1 --- 0x4
  Adresse Internet      Adresse physique      Type
  192.168.125.2        00-e0-4c-68-04-2c     dynamique [adresse mac de mon binome]
  192.168.125.63        ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
[...]
Interface : 10.5.1.10 --- 0x7
  Adresse Internet      Adresse physique      Type
  10.5.1.255            ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.177.1 --- 0xf
  Adresse Internet      Adresse physique      Type
  192.168.177.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.33.18.125 --- 0x10
  Adresse Internet      Adresse physique      Type
  10.33.19.254          00-c0-e7-e0-04-4e     dynamique
  10.33.19.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```


🌞 **Manipuler la table ARP**

executer la commande en administrateur 
```
PS C:\Windows\system32> arp -d
```

la table arp c'est vidé

```
PS C:\Windows\system32> arp -a

Interface : 192.168.125.1 --- 0x4
  Adresse Internet      Adresse physique      Type
  192.168.125.2        00-e0-4c-68-04-2c     dynamique
  224.0.0.22            01-00-5e-00-00-16     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 10.5.1.10 --- 0x7
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique

Interface : 192.168.177.1 --- 0xf
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique

Interface : 10.33.18.125 --- 0x10
  Adresse Internet      Adresse physique      Type
  10.33.19.254          00-c0-e7-e0-04-4e     dynamique
  224.0.0.22            01-00-5e-00-00-16     statique
```

et là on constate qu'il y a moins déléments dans la table 


Si on ping une adresse on constate que des éléments ce rajoute dans la table arp

```
PS C:\Windows\system32> ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=31 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=26 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=30 ms TTL=114

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 3, reçus = 3, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 26ms, Maximum = 31ms, Moyenne = 29ms


PS C:\Windows\system32> arp -a

Interface : 192.168.125.1 --- 0x4
  Adresse Internet      Adresse physique      Type
  192.168.125.2        00-e0-4c-68-04-2c     dynamique
  224.0.0.22            01-00-5e-00-00-16     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique

Interface : 10.5.1.10 --- 0x7
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.177.1 --- 0xf
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.33.18.125 --- 0x10
  Adresse Internet      Adresse physique      Type
  10.33.18.194          78-4f-43-87-f5-11     dynamique
  10.33.19.254          00-c0-e7-e0-04-4e     dynamique
  224.0.0.22            01-00-5e-00-00-16     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```


🌞 **Wireshark it**

Mon pc envoie un message dans tout le réseau pour demandé a qui appartient l'ip 192.168.125.2
Le broadcast répond que la machine avec l'ip 192.168.125.2 a pour MAC : 54:e1:ad:98:7d:cd.
Dans le cas ici présent le broadcast ne me répond pas c'est la machine qui me répond directement, car mon binome m'a ping après que j'ai vidé ma table ARP, sa machine connait donc déjà ma MAC et me répond directement.


* [ARP](/Packet%20ARP.pcapng)

## III. DHCP you too my brooo


🌞 **Wireshark it**

* [DHCP](/Packet%20DHCP.pcapng)

## IV. Avant-goût TCP et UDP

🌞 **Wireshark it**